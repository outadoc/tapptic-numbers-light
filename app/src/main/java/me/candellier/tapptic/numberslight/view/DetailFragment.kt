/*
 * Copyright (c) 2018 Baptiste Candellier (for Tapptic)
 */

package me.candellier.tapptic.numberslight.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.details_fragment.*
import me.candellier.tapptic.numberslight.App
import me.candellier.tapptic.numberslight.R
import me.candellier.tapptic.numberslight.component.DaggerRepositoryComponent
import me.candellier.tapptic.numberslight.makeHttps
import me.candellier.tapptic.numberslight.model.Status
import me.candellier.tapptic.numberslight.viewmodel.MainViewModel
import me.candellier.tapptic.numberslight.viewmodel.MainViewModelFactory
import javax.inject.Inject

class DetailFragment : Fragment() {
    @Inject
    lateinit var factory: MainViewModelFactory

    private lateinit var viewModel: MainViewModel

    companion object {
        fun newInstance() = DetailFragment()
    }

    init {
        DaggerRepositoryComponent
                .builder().build()
                .inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.details_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        // Get ViewModel singleton (either from factory, or the one created by ListFragment)
        viewModel = activity?.run {
            ViewModelProvider(application as App, factory).get(MainViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        // Observe details and when they change, update the UI
        viewModel.details.observe(this, Observer { res ->
            when (res?.status) {
                Status.SUCCESS -> {
                    res.data?.run {
                        itemDetailNameLabel.text = name
                        itemDetailTextLabel.text = text
                        Picasso.get().load(image.makeHttps()).into(itemDetailImage)
                    }
                }

                Status.LOADING -> {
                    Toast.makeText(context, "Loading...", Toast.LENGTH_SHORT).show()
                }

                Status.ERROR -> {
                    context?.let {
                        AlertDialog.Builder(it)
                                .setTitle(R.string.loading_error)
                                .setMessage(res.message)
                                .setCancelable(true)
                                .create().show()
                    }
                }

                null -> {
                    Toast.makeText(context, "No result", Toast.LENGTH_LONG).show()
                }
            }
        })
    }
}