/*
 * Copyright (c) 2018 Baptiste Candellier (for Tapptic)
 */

package me.candellier.tapptic.numberslight.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import me.candellier.tapptic.numberslight.R

class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_detail)

        title = getString(R.string.detail_title)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (savedInstanceState == null) {
            val fragment = DetailFragment.newInstance()
            supportFragmentManager.beginTransaction()
                    .add(R.id.item_detail_container, fragment)
                    .commit()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }

    }
}
