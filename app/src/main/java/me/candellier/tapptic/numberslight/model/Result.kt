/*
 * Copyright (c) 2018 Baptiste Candellier (for Tapptic)
 */

package me.candellier.tapptic.numberslight.model

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}

/**
 * Wrapper class around a webservice result.
 * Allows the user component to monitor the request status.
 */
data class Result<out T>(val status: Status, val data: T?, val message: String?) {
    companion object {
        fun <T> success(data: T?): Result<T> {
            return Result(Status.SUCCESS, data, null)
        }

        fun <T> error(msg: String, data: T?): Result<T> {
            return Result(Status.ERROR, data, msg)
        }

        fun <T> loading(): Result<T> {
            return Result(Status.LOADING, null, null)
        }
    }
}