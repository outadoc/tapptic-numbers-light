/*
 * Copyright (c) 2018 Baptiste Candellier (for Tapptic)
 */

package me.candellier.tapptic.numberslight.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import me.candellier.tapptic.numberslight.repositories.ItemRepository
import javax.inject.Inject

class MainViewModelFactory @Inject constructor(val repository: ItemRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            MainViewModel(this.repository) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }
}