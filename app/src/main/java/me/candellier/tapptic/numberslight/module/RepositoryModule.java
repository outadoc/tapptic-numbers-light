/*
 * Copyright (c) 2018 Baptiste Candellier (for Tapptic)
 */

package me.candellier.tapptic.numberslight.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.candellier.tapptic.numberslight.repositories.ItemRepository;
import me.candellier.tapptic.numberslight.repositories.ItemRepositoryImpl;
import me.candellier.tapptic.numberslight.rest.ItemService;
import me.candellier.tapptic.numberslight.viewmodel.MainViewModelFactory;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class RepositoryModule {

    @Provides
    @Singleton
    public Retrofit provideRetrofit() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        // Create REST service
        return new Retrofit.Builder()
                .baseUrl("https://dev.tapptic.com/")
                .addConverterFactory(
                        GsonConverterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    public ItemRepository provideRepository(Retrofit retrofit) {
        return new ItemRepositoryImpl(retrofit.create(ItemService.class));
    }

    @Provides
    @Singleton
    public MainViewModelFactory provideMainViewModelFactory(ItemRepository repository) {
        return new MainViewModelFactory(repository);
    }
}
