/*
 * Copyright (c) 2018 Baptiste Candellier (for Tapptic)
 */

package me.candellier.tapptic.numberslight.repositories

import me.candellier.tapptic.numberslight.model.Item
import me.candellier.tapptic.numberslight.model.ItemDetails
import retrofit2.Call

interface ItemRepository {
    fun getItems(): Call<List<Item>>
    fun getItemDetailsByName(name: String): Call<ItemDetails>
}
