/*
 * Copyright (c) 2018 Baptiste Candellier (for Tapptic)
 */

package me.candellier.tapptic.numberslight.component

import dagger.Component
import me.candellier.tapptic.numberslight.module.RepositoryModule
import me.candellier.tapptic.numberslight.view.DetailFragment
import me.candellier.tapptic.numberslight.view.ListFragment
import me.candellier.tapptic.numberslight.view.MainActivity
import javax.inject.Singleton

@Singleton
@Component(modules = [RepositoryModule::class])
interface RepositoryComponent {
    fun inject(frag: ListFragment)
    fun inject(frag: DetailFragment)
    fun inject(activity: MainActivity)
}