/*
 * Copyright (c) 2018 Baptiste Candellier (for Tapptic)
 */

package me.candellier.tapptic.numberslight.rest

import me.candellier.tapptic.numberslight.model.Item
import me.candellier.tapptic.numberslight.model.ItemDetails
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ItemService {
    @GET("test/json.php")
    fun getItems(): Call<List<Item>>

    @GET("test/json.php")
    fun getItemDetailsByName(@Query("name") name: String): Call<ItemDetails>
}
