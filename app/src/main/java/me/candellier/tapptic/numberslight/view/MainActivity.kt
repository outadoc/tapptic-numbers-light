/*
 * Copyright (c) 2018 Baptiste Candellier (for Tapptic)
 */

package me.candellier.tapptic.numberslight.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import me.candellier.tapptic.numberslight.App
import me.candellier.tapptic.numberslight.R
import me.candellier.tapptic.numberslight.component.DaggerRepositoryComponent
import me.candellier.tapptic.numberslight.viewmodel.MainViewModel
import me.candellier.tapptic.numberslight.viewmodel.MainViewModelFactory
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var factory: MainViewModelFactory

    private lateinit var viewModel: MainViewModel

    init {
        DaggerRepositoryComponent
                .builder().build()
                .inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProvider(application as App, factory).get(MainViewModel::class.java)

        viewModel.selectedItem.observe(this, Observer { item ->
            if (fragmentContainerDetail == null) {
                // No detail fragment; we're on a "phone" layout
                item?.let {
                    val intent = Intent(this, DetailActivity::class.java)
                    startActivity(intent)
                }
            }
        })

        fragmentContainerDetail?.let {
            // Setup tablet layout
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragmentContainerDetail, DetailFragment.newInstance())
                    .commit()
        }

        if (savedInstanceState != null) {
            // We don't want overlapping fragments and multiple listeners :)
            return
        }

        // Show main list in main fragment
        supportFragmentManager
                .beginTransaction()
                .add(R.id.fragment_container, ListFragment.newInstance())
                .commit()
    }
}
