/*
 * Copyright (c) 2018 Baptiste Candellier (for Tapptic)
 */

package me.candellier.tapptic.numberslight.view

import android.arch.lifecycle.MutableLiveData
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import me.candellier.tapptic.numberslight.R
import me.candellier.tapptic.numberslight.makeHttps
import me.candellier.tapptic.numberslight.model.Item

class ItemAdapter(private val items: List<Item>) : RecyclerView.Adapter<ItemAdapter.ItemViewHolder>() {
    class ItemViewHolder(val container: View) : RecyclerView.ViewHolder(container)

    val selectedItem = MutableLiveData<Item>()

    override fun onCreateViewHolder(parent: ViewGroup, index: Int): ItemViewHolder {
        // create a new view from our awesome resource
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.view_item_row, parent, false)

        return ItemViewHolder(view)
    }

    override fun getItemCount(): Int = items.count()

    override fun onBindViewHolder(vh: ItemViewHolder, index: Int) {
        items[index].let { currentItem ->
            val itemNameLabel = vh.container.findViewById<TextView>(R.id.itemNameLabel)
            val itemImage = vh.container.findViewById<ImageView>(R.id.itemImage)

            // Set item label
            itemNameLabel.text = currentItem.name

            // Load image into the row asynchronously using Picasso
            Picasso.get().load(currentItem.image.makeHttps()).into(itemImage)

            vh.container.isSelected = currentItem == selectedItem.value

            // When we click on the view, set the selectedItem property
            vh.container.setOnClickListener {
                val oldSelectedIndex = items.indexOf(selectedItem.value)
                selectedItem.value = currentItem

                // Set previously selected item as not selected anymore
                notifyItemChanged(oldSelectedIndex)

                // Set this item as selected
                notifyItemChanged(index)
            }
        }
    }
}