/*
 * Copyright (c) 2018 Baptiste Candellier (for Tapptic)
 */

package me.candellier.tapptic.numberslight.model

data class ItemDetails(val name: String, val text: String, val image: String) 