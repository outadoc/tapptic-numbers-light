/*
 * Copyright (c) 2018 Baptiste Candellier (for Tapptic)
 */

package me.candellier.tapptic.numberslight.repositories

import dagger.Module
import me.candellier.tapptic.numberslight.model.Item
import me.candellier.tapptic.numberslight.model.ItemDetails
import me.candellier.tapptic.numberslight.rest.ItemService
import retrofit2.Call

@Module
class ItemRepositoryImpl(private val itemService: ItemService) : ItemRepository {
    override fun getItems(): Call<List<Item>> =
            itemService.getItems()

    override fun getItemDetailsByName(name: String): Call<ItemDetails> =
            itemService.getItemDetailsByName(name)
}