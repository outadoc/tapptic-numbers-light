/*
 * Copyright (c) 2018 Baptiste Candellier (for Tapptic)
 */

package me.candellier.tapptic.numberslight.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import android.widget.Toast
import kotlinx.android.synthetic.main.main_list_fragment.*
import me.candellier.tapptic.numberslight.App
import me.candellier.tapptic.numberslight.R
import me.candellier.tapptic.numberslight.component.DaggerRepositoryComponent
import me.candellier.tapptic.numberslight.model.Item
import me.candellier.tapptic.numberslight.model.Status
import me.candellier.tapptic.numberslight.viewmodel.MainViewModel
import me.candellier.tapptic.numberslight.viewmodel.MainViewModelFactory
import javax.inject.Inject


class ListFragment : Fragment() {
    @Inject
    lateinit var factory: MainViewModelFactory

    private lateinit var viewModel: MainViewModel

    private val itemsHolder: MutableList<Item> = mutableListOf()

    companion object {
        fun newInstance() = ListFragment()
    }

    init {
        DaggerRepositoryComponent
                .builder().build()
                .inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.toolbar_menu, menu)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.main_list_fragment, container, false)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.item_refresh -> {
                viewModel.loadList()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = activity?.run {
            ViewModelProvider(application as App, factory).get(MainViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        viewModel.items.observe(this, Observer { res ->
            when (res?.status) {
                Status.SUCCESS -> {
                    itemsHolder.removeAll { true }
                    itemsHolder.addAll(res.data ?: emptyList())
                    itemsListContainer.adapter?.notifyDataSetChanged()
                }

                Status.LOADING -> {
                    Toast.makeText(context, "Loading...", Toast.LENGTH_SHORT).show()
                }

                Status.ERROR -> {
                    context?.let {
                        AlertDialog.Builder(it)
                                .setTitle(R.string.loading_error)
                                .setMessage(res.message)
                                .setCancelable(true)
                                .setPositiveButton(android.R.string.ok, null)
                                .create().show()
                    }
                }

                null -> {
                    Toast.makeText(context, "No result", Toast.LENGTH_LONG).show()
                }
            }
        })

        val itemAdapter = ItemAdapter(itemsHolder)

        itemAdapter.selectedItem.observe(this, Observer { item ->
            // When the adapter's selected item changes, reflect this in the ViewModel
            viewModel.selectedItem.value = item
        })

        itemsListContainer.apply {
            adapter = itemAdapter
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }

        if (viewModel.items.value == null) {
            viewModel.loadList()
        }
    }
}

