/*
 * Copyright (c) 2018 Baptiste Candellier (for Tapptic)
 */

package me.candellier.tapptic.numberslight.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import me.candellier.tapptic.numberslight.callback
import me.candellier.tapptic.numberslight.model.Item
import me.candellier.tapptic.numberslight.model.ItemDetails
import me.candellier.tapptic.numberslight.model.Result
import me.candellier.tapptic.numberslight.repositories.ItemRepository

class MainViewModel(private val repository: ItemRepository) : ViewModel() {
    private val _items = MutableLiveData<Result<List<Item>>>()
    val items: LiveData<Result<List<Item>>>
        get() = _items

    val selectedItem = MutableLiveData<Item>()

    private val _details = MutableLiveData<Result<ItemDetails>>()
    val details: LiveData<Result<ItemDetails>>
        get() = _details

    init {
        selectedItem.observeForever { item ->
            item?.let { loadItemDetails(it) }
        }
    }

    /**
     * Loads or reloads the list of items.
     */
    fun loadList() {
        _items.postValue(Result.loading())

        // Start loading items
        repository.getItems()
                .enqueue(callback { throwable, response ->
                    response?.let {
                        // Response OK, update the LiveData item
                        _items.postValue(Result.success(response.body()))
                    }

                    throwable?.let {
                        // Error, set error state with the error message
                        _items.postValue(Result.error(throwable.message.toString(), response?.body()))
                    }
                })
    }

    /**
     * Loads details for the given [item].
     */
    private fun loadItemDetails(item: Item) {
        _details.postValue(Result.loading())

        repository.getItemDetailsByName(item.name)
                .enqueue(callback { throwable, response ->
                    response?.let {
                        // Response OK, update the LiveData item
                        _details.postValue(Result.success(response.body()))
                    }

                    throwable?.let {
                        // Error, set error state with the error message
                        _details.postValue(Result.error(response?.errorBody().toString(), response?.body()))
                    }
                })
    }
}
