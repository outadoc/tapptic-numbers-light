/*
 * Copyright (c) 2018 Baptiste Candellier (for Tapptic)
 */

package me.candellier.tapptic.numberslight

import android.app.Application
import android.arch.lifecycle.ViewModelStore
import android.arch.lifecycle.ViewModelStoreOwner

class App : Application(), ViewModelStoreOwner {
    private val appViewModelStore: ViewModelStore by lazy {
        ViewModelStore()
    }

    override fun getViewModelStore(): ViewModelStore = appViewModelStore
}