/*
 * Copyright (c) 2018 Baptiste Candellier (for Tapptic)
 */

package me.candellier.tapptic.numberslight

import android.support.test.runner.AndroidJUnit4
import me.candellier.tapptic.numberslight.model.Status
import me.candellier.tapptic.numberslight.viewmodel.MainViewModel
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ViewModelTests {
    private lateinit var repository: RepositoryMock
    private lateinit var vm: MainViewModel

    @Before
    fun setup() {
        repository = RepositoryMock()
        vm = MainViewModel(repository)
    }

    @Test fun testListLoad() {
        vm.loadList()

        Thread.sleep(1000)

        assertNotNull(vm.items.value)

        assertEquals(Status.SUCCESS, vm.items.value!!.status)
        assertNotNull(vm.items.value!!.data)
        assertEquals(3, vm.items.value!!.data!!.size)

        assertEquals("1", vm.items.value!!.data!![0].name)
        assertEquals("http://inu.tapptic.com/test/image.php?text=%E4%B8%80", vm.items.value!!.data!![0].image)
    }

    @Test fun testListSelect() {
        vm.loadList()

        Thread.sleep(1000)

        assertNotNull(vm.items.value)

        vm.selectedItem.postValue(vm.items.value!!.data!![1])

        Thread.sleep(1000)

        assertEquals(Status.SUCCESS, vm.details.value!!.status)
        assertNotNull(vm.details.value!!.data)

        assertEquals("2", vm.details.value!!.data!!.name)
        assertEquals("http://inu.tapptic.com/test/image.php?text=%E4%BA%8C&size=6", vm.details.value!!.data!!.image)
    }
}
