/*
 * Copyright (c) 2018 Baptiste Candellier (for Tapptic)
 */

package me.candellier.tapptic.numberslight

import me.candellier.tapptic.numberslight.model.Item
import me.candellier.tapptic.numberslight.model.ItemDetails
import me.candellier.tapptic.numberslight.repositories.ItemRepository
import okhttp3.Request
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ListCallMock(private val response: List<Item>) : Call<List<Item>> {
    override fun enqueue(callback: Callback<List<Item>>) {
        callback.onResponse(this, Response.success(response))
    }

    override fun isExecuted(): Boolean = true
    override fun clone(): Call<List<Item>> = throw NotImplementedError()
    override fun isCanceled(): Boolean = false
    override fun cancel() = throw NotImplementedError()
    override fun execute(): Response<List<Item>> = throw NotImplementedError()
    override fun request(): Request = throw NotImplementedError()
}

class DetailsCallMock(private val response: ItemDetails) : Call<ItemDetails> {
    override fun enqueue(callback: Callback<ItemDetails>) {
        callback.onResponse(this, Response.success(response))
    }

    override fun isExecuted(): Boolean = true
    override fun clone(): Call<ItemDetails> = throw NotImplementedError()
    override fun isCanceled(): Boolean = false
    override fun cancel() = throw NotImplementedError()
    override fun execute(): Response<ItemDetails> = throw NotImplementedError()
    override fun request(): Request = throw NotImplementedError()
}

class RepositoryMock : ItemRepository {
    override fun getItems(): Call<List<Item>> {
        return ListCallMock(listOf(
                Item("1", "http://inu.tapptic.com/test/image.php?text=%E4%B8%80"),
                Item("2", "http://inu.tapptic.com/test/image.php?text=%E4%BA%8C"),
                Item("3", "http://inu.tapptic.com/test/image.php?text=%E4%B8%89")
        ))
    }

    override fun getItemDetailsByName(name: String): Call<ItemDetails> {
        return when (name){
            "1" -> DetailsCallMock(ItemDetails("1", "Ichi", "http://inu.tapptic.com/test/image.php?text=%E4%B8%80&size=6"))
            "2" -> DetailsCallMock(ItemDetails("2", "Ni", "http://inu.tapptic.com/test/image.php?text=%E4%BA%8C&size=6"))
            "3" -> DetailsCallMock(ItemDetails("3", "San", "http://inu.tapptic.com/test/image.php?text=%E4%B8%89&size=6"))
            else -> throw NotImplementedError()
        }
    }

}