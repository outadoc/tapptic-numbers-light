# Tapptic - Numbers Light

Author: Baptiste CANDELLIER

Total time spent: 07:30 *(whew)*

## Contents

- Written in Kotlin
- Architecture: MVVM (Model-View-ViewModel)
- Tech specs and functional specs-compliant
- Some unit tests
- Unconditional HTTP logs
- 1 Java class: `me.candellier.tapptic.numberslight.RepositoryComponent`
- Libs used: Retrofit, Dagger, Android Support libs, Picasso, JUnit
